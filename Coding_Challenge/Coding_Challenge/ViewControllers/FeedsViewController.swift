//
//  FeedsViewController.swift
//  Coding_Challenge
//
//  Created by Waqas Haider Sheikh on 15/09/2021.
//

import UIKit
import SVProgressHUD
import ESPullToRefresh

class FeedsViewController: UIViewController {
    private let feedsViewModel = FeedsViewModel()
    var feeds = [Feed]() {
        didSet {
            reloadTableView()
        }
    }
    var isLoading = false
    
    // MARK:- IBOutlets
    var tableView: UITableView? = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // configure view
        configureView()
        
        // configure load more
        configureLoadMore()
        
        // load feeds
        loadFeeds()
    }
    
    private func loadFeeds() {
        SVProgressHUD.show(withStatus: "Loading...")
        feedsViewModel.loadFeeds().done { [weak self] feeds in
            // dismiss
            SVProgressHUD.dismiss()
            
            // update model
            self?.feeds = feeds
        }.catch { error in
            guard let error = error as? AppError else {
                SVProgressHUD.dismiss()
                return
            }
            SVProgressHUD.showError(withStatus: error.message)
        }
    }
    
    private func loadMoreFeeds() {
        guard let lastFeed = feeds.last else { return }
        SVProgressHUD.show(withStatus: "Loading...")
        feedsViewModel.loadMoreFeeds(with: lastFeed.name).done { [weak self] feeds in
            // dismiss
            SVProgressHUD.dismiss()
            
            // update model with more feeds
            self?.feeds.append(contentsOf: feeds)
            
            // update isLoading flag
            self?.isLoading = false
        }.catch { [weak self] error in
            guard let error = error as? AppError else {
                // dismiss
                SVProgressHUD.dismiss()
                
                // update isLoading flag
                self?.isLoading = false
                return
            }
            
            // dismiss with with error
            SVProgressHUD.showError(withStatus: error.message)
            
            // update isLoading flag
            self?.isLoading = false
        }
    }
    
    private func reloadTableView() {
        guard !feeds.isEmpty else {
            tableView?.showNoDataView(with: "No feeds found")
            return
        }
        tableView?.hideNoDataView()
    }
    
    private func configureView() {
        guard let tableView = tableView else { return }
        view.addSubview(tableView)
        tableView.fillInSuperView()
        
        // Don't forget to set dataSource and delegate for table
        tableView.delegate = self
        tableView.dataSource = self
        
        // Set automatic dimensions for row height
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = UITableView.automaticDimension
        
        // register cell
        tableView.register(FeedCell.self, forCellReuseIdentifier: FeedCell.identifier)
    }
    
    private func configureLoadMore() {
        tableView?.es.addInfiniteScrolling { [weak self] in
            // check for loading status
            
            guard self?.isLoading == false else {
                // stop loading more
                self?.tableView?.es.stopLoadingMore()
                return
            }
            
            // update isLoading flag
            self?.isLoading = true
            
            // load more feeds
            self?.loadMoreFeeds()
            
            // stop loading more
            self?.tableView?.es.stopLoadingMore()
        }
    }
}

// MARK:- TableView DataSource & Delegates
extension FeedsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        feeds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let feedCell = tableView.dequeueReusableCell(withIdentifier: FeedCell.identifier) as? FeedCell else { return UITableViewCell() }
        
        // update cell
        guard let feed = feeds[optional: indexPath.row] else { return UITableViewCell() }
        feedCell.feed = feed
        return feedCell
    }
    
    // UITableViewAutomaticDimension calculates height of label contents/text
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

