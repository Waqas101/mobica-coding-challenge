//
//  FeedCell.swift
//  Coding_Challenge
//
//  Created by Waqas Haider Sheikh on 15/09/2021.
//

import Foundation
import UIKit
import SDWebImage

class FeedCell: UITableViewCell {
    var feed: Feed? {
        didSet {
            configureView()
        }
    }
    
    // cell outlets
    private let imageViewThumbnail: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private let labelTitle: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 17.0)
        label.textAlignment = .left
        label.numberOfLines = 0
        label.text = ""
        label.textColor = .black
        label.minimumScaleFactor = 0.5
        return label
    }()
    
    private let labelScore: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 17.0)
        label.textAlignment = .left
        label.numberOfLines = 1
        label.text = ""
        label.textColor = .black
        label.minimumScaleFactor = 0.5
        return label
    }()
    
    private let labelTotalCommentsCount: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 17.0)
        label.textAlignment = .left
        label.numberOfLines = 1
        label.text = ""
        label.textColor = .black
        label.minimumScaleFactor = 0.5
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // setup view
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        // horizontal stack to contain comments count and score
        let stackViewHorizontal = UIStackView(arrangedSubviews: [labelTotalCommentsCount, labelScore])
        stackViewHorizontal.axis = .horizontal
        stackViewHorizontal.spacing = 5.0
        
        // set resistance priority to low for thumbnail imageView
        imageViewThumbnail.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        imageViewThumbnail.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
        
        // vertical stack to contain title and above horizontal stack
        let stackViewVertical = UIStackView(arrangedSubviews: [labelTitle, stackViewHorizontal])
        stackViewVertical.axis = .vertical
        stackViewVertical.spacing = 5.0
        
        // main stack to contain all stacks
        let stackViewMain = UIStackView(arrangedSubviews: [imageViewThumbnail, stackViewVertical])
        stackViewMain.axis = .vertical
        stackViewMain.spacing = 5.0
        
        // add subview
        addSubview(stackViewMain)
        
        // fill in main stack to superview
        stackViewMain.fillInSuperView(top: 5.0, bottom: 5.0, leading: 5.0, trailing: 5.0)
    }
    
    private func configureView() {
        guard let feed = feed else { return }
        imageViewThumbnail.sd_setImage(with: URL(string: feed.thumbnail), placeholderImage: #imageLiteral(resourceName: "PlaceHolderIcon"))
        labelTitle.text = feed.title
        labelTotalCommentsCount.text = "Comments: \(feed.totalCommentsCount)"
        labelScore.text = "Score: \(feed.score)"
    }
}
