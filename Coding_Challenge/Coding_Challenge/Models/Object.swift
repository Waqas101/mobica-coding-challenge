//
//  Object.swift
//  ISAssessment
//
//  Created by Waqas Haider Sheikh on 10/12/2020.
//  Copyright © 2020 Waqas Haider Sheikh. All rights reserved.
//

import Foundation
import ObjectMapper

class Object: Mappable {
    required init?(map: Map) {
        mapping(map: map)
    }
    func mapping(map: Map) {}
}
