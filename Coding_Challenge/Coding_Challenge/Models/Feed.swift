//
//  Feed.swift
//  Coding_Challenge
//
//  Created by Waqas Haider Sheikh on 15/09/2021.
//

import Foundation
import ObjectMapper

class Feed: Object {
    var thumbnail = ""
    var thumbnailHeight = 0
    var thumbnailWidth = 0
    var title = ""
    var score = 0
    var totalCommentsCount = 0
    var name = ""
    
    override func mapping(map: Map) {
        thumbnail          <- map["data.thumbnail"]
        thumbnailHeight    <- map["data.thumbnail_height"]
        thumbnailWidth     <- map["data.thumbnail_width"]
        title              <- map["data.title"]
        score              <- map["data.score"]
        totalCommentsCount <- map["data.num_comments"]
        name               <- map["data.name"]
    }
}

/*
 title : "Hello from Crytek! We're giving away 20 Crysis Remastered Steam Keys"
 score : 6116
 thumbnail : "https://b.thumbs.redditmedia.com/Kbasy_rVMWG1iqvnFaTrSEXPl5uuc14sLHBHogpXtUk.jpg"
 thumbnail_height : 73
 thumbnail_width : 140
 num_comments : 17706
 */
