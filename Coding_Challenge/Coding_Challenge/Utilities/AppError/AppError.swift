//
//  AppError.swift
//  ISAssessment
//
//  Created by Waqas Haider Sheikh on 11/12/2020.
//  Copyright © 2020 Waqas Haider Sheikh. All rights reserved.
//

import Foundation

class AppError: NSError {
    var message = ""
    
    init (_ code: Int, domain: String, message: String) {
        super.init(domain: domain, code: code, userInfo: nil)
        self.message = message
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension AppError {
    enum Errors: String, Error {
        
        case emptyFields = "Please fill all required fields."
        case inValidResponse = "Invalid response, Please try again."
        case unAuthorizedAccess = "Access denied, Please re-Login the application"
        case invalidRequest = "Request cannot be completed at this moment."
        case unKnown = "Something went wrong!"
        
        var message: String {
            return rawValue
        }
    }
}
