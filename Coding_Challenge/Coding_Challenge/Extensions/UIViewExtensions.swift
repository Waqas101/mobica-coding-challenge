//
//  UIViewExtensions.swift
//  ISAssessment
//
//  Created by Waqas Haider Sheikh on 10/12/2020.
//  Copyright © 2020 Waqas Haider Sheikh. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
}

extension UIView {
    func disableAutoresizingMask() {
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    @discardableResult
    func centerHorizontally(with padding: Int = 0) -> [NSLayoutConstraint] {
        
        // can't set constraints if we dont have a superview
        guard let superview = superview else {
            return []
        }
        
        disableAutoresizingMask()
        let constraints = NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: superview, attribute: .centerX, multiplier: 1, constant: CGFloat(padding))
        superview.addConstraint(constraints)
        return [constraints]
    }
    
    @discardableResult
    func centerVertically(with padding: Int = 0) -> [NSLayoutConstraint] {
        
        // can't set constraints if we dont have a superview
        guard let superview = superview else {
            return []
        }
        
        disableAutoresizingMask()
        let constraints = NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: superview, attribute: .centerY, multiplier: 1, constant: CGFloat(padding))
        superview.addConstraint(constraints)
        return [constraints]
    }
    
    @discardableResult
    func setLeftMargin(margin: Int = 15, with relation: NSLayoutConstraint.Relation = .equal) -> [NSLayoutConstraint] {
        return applyConstraints(for: margin, with: "H:|-(\(relation.visualValue)margin)-[view]")
    }
    
    @discardableResult
    func setRightMargin(margin: Int = 15, with relation: NSLayoutConstraint.Relation = .equal) -> [NSLayoutConstraint] {
        return applyConstraints(for: margin, with: "H:[view]-(\(relation.visualValue)margin)-|")
    }
    
    @discardableResult
    func setTopMargin(margin: Int = 15, with relation: NSLayoutConstraint.Relation = .equal) -> [NSLayoutConstraint] {
        return applyConstraints(for: margin, with: "V:|-(\(relation.visualValue)margin)-[view]")
    }
    
    @discardableResult
    func setBottomMargin(margin: Int = 15, with relation: NSLayoutConstraint.Relation = .equal) -> [NSLayoutConstraint] {
        return applyConstraints(for: margin, with: "V:[view]-(\(relation.visualValue)margin)-|")
    }
    
    func applyConstraints(for margin: Int, with visualFormat: String) -> [NSLayoutConstraint] {
        
        // can't set constraints if we dont have a superview
        guard let superview = superview else {
            return []
        }
        
        disableAutoresizingMask()
        let views = ["view": self]
        let metrics = ["margin": margin, "value": margin]
        
        let constraints = NSLayoutConstraint.constraints(withVisualFormat: visualFormat, options: NSLayoutConstraint.FormatOptions(), metrics: metrics, views: views)
        superview.addConstraints(constraints)
        return constraints
    }
    
    // autolayout to fill view
    func fillInSuperView(top: CGFloat = 0.0, bottom: CGFloat = 0.0, leading: CGFloat = 0.0, trailing: CGFloat = 0.0) {
        
        // add basic layout constraints for the view
        guard let superview = superview else { return }
        
        disableAutoresizingMask()
        
        superview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-\(leading)-[view]-\(trailing)-|", options: [], metrics: nil, views: ["view": self]))
        superview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(top)-[view]-\(bottom)-|", options: [], metrics: nil, views: ["view": self]))
    }
    
    func setWidthConstraint(relation: NSLayoutConstraint.Relation = .equal, multiplier: CGFloat  = 1, constant: CGFloat = 1) {
        addConstraint(NSLayoutConstraint(item: self, attribute: .width, relatedBy: relation, toItem: nil, attribute: .width, multiplier: multiplier, constant: constant))
    }
    
    func setHeightConstraint(relation: NSLayoutConstraint.Relation = .equal, multiplier: CGFloat  = 1, constant: CGFloat = 1) {
        addConstraint(NSLayoutConstraint(item: self, attribute: .height, relatedBy: relation, toItem: nil, attribute: .height, multiplier: multiplier, constant: constant))
    }
    
    @discardableResult
    func setEqualWidthConstraint(relation: NSLayoutConstraint.Relation = .equal, multiplier: CGFloat  = 1, constant: CGFloat = 1) -> NSLayoutConstraint? {
        return addConstraint(attribute: .width, relation: relation, multiplier: multiplier, constant: constant)
    }
    
    @discardableResult
    func setEqualHeightConstraint(relation: NSLayoutConstraint.Relation = .equal, multiplier: CGFloat  = 1, constant: CGFloat = 1) -> NSLayoutConstraint? {
        return addConstraint(attribute: .height, relation: relation, multiplier: multiplier, constant: constant)
    }
    
    @discardableResult
    func aspectRation(_ ratio: CGFloat) -> NSLayoutConstraint? {
        return NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: self, attribute: .width, multiplier: ratio, constant: 0)
    }
    
    @discardableResult
    func addConstraint(attribute: NSLayoutConstraint.Attribute, relation: NSLayoutConstraint.Relation = .equal, multiplier: CGFloat  = 1, constant: CGFloat = 1) -> NSLayoutConstraint? {
        
        // add basic layout constraints for the view
        guard let superView = superview else { return nil }
        disableAutoresizingMask()
        let constraint = NSLayoutConstraint(item: self, attribute: attribute, relatedBy: relation, toItem: superView, attribute: attribute, multiplier: multiplier, constant: constant)
        superView.addConstraint(constraint)
        return constraint
    }
}

extension NSLayoutConstraint.Relation {
    var visualValue: String {
        switch self {
        case .equal: return ""
        case .lessThanOrEqual: return "<="
        case .greaterThanOrEqual: return ">="
        @unknown default: fatalError()
        }
    }
}
