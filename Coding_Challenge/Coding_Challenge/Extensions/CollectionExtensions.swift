//
//  CollectionExtensions.swift
//  ISAssessment
//
//  Created by Waqas Haider on 11/12/2020.
//  Copyright © 2020 Waqas Haider Sheikh. All rights reserved.
//

import Foundation

extension Collection {
    subscript(optional index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
