//
//  UITableViewExtensions.swift
//  ISAssessment
//
//  Created by Waqas Haider on 11/12/2020.
//  Copyright © 2020 Waqas Haider Sheikh. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    func showNoDataView(with message: String) {
        
        separatorStyle = .none
        let labelNoData = UILabel(frame: CGRect(x: 0, y: 0, width: frame.width, height: 100))
        labelNoData.text = message
        labelNoData.textAlignment = .center
        backgroundView = labelNoData
        reloadData()
    }
    
    func hideNoDataView() {
        separatorStyle = .singleLine
        backgroundView = nil
        reloadData()
    }
}
