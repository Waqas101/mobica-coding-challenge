//
//  StringExtensions.swift
//  ISAssessment
//
//  Created by Waqas Haider Sheikh on 11/12/2020.
//  Copyright © 2020 Waqas Haider Sheikh. All rights reserved.
//

import Foundation

extension String {
    
    func trimWhiteSpaces() -> Self {
        return trimmingCharacters(in: .whitespaces)
    }
    
    func contains(_ text: String) -> Bool {
        return range(of: text, options: .caseInsensitive) != nil
    }
}
