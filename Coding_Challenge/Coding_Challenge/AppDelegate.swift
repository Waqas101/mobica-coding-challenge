//
//  AppDelegate.swift
//  Coding_Challenge
//
//  Created by Waqas Haider Sheikh on 15/09/2021.
//

import UIKit
import SwiftyBeaver
import SVProgressHUD

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
    
    private func start() {
        
        // SVProgressHUD
        configureSVProgressHUD()
        
        // configure swifty beaver
        configureSwiftyBeaver()
        
        // add observer for network reachability
        configureReachabilityObserver()
    }
    
    private func configureSVProgressHUD() {
        SVProgressHUD.setDefaultStyle(.custom)
        SVProgressHUD.setDefaultMaskType(.gradient)
        SVProgressHUD.setMinimumDismissTimeInterval(0.5)
    }
    
    private func configureSwiftyBeaver() {
        // add log destinations
        let console = ConsoleDestination()
        
        // use custom format and set console output to short time, log level & message
        console.format = "$DHH:mm:ss$d $L $M"
        // or use this for JSON output: console.format = "$J"
        
        // add the destinations to SwiftyBeaver
        Log.addDestination(console)
    }
    
    private func configureReachabilityObserver() {
        // when internet is reachable
        NetworkManager.sharedInstance.reachability.whenReachable = { _ in
            DispatchQueue.main.async {
                SVProgressHUD.showInfo(withStatus: "Connected to Network")
            }
        }
        
        // when internet is not reachable
        NetworkManager.sharedInstance.reachability.whenUnreachable = { _ in
            DispatchQueue.main.async {
                SVProgressHUD.showInfo(withStatus: "Network not available")
            }
        }
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
}

