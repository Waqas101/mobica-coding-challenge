//
//  FeedsAPIService.swift
//  Coding_Challenge
//
//  Created by Waqas Haider Sheikh on 15/09/2021.
//

import Foundation
import Alamofire
import PromiseKit
import ObjectMapper

class FeedsAPIService {
    func fetchFeeds() -> Promise<[Feed]> {
        return Promise { resolver in
            Router.fetchFeeds.request { (response: DataResponse<SuccessResponse>) in
                guard response.error == nil else {
                    resolver.reject(response.error!)
                    return
                }
                guard let result = response.result.value else {
                    let error = AppError(1001, domain: "Error", message: AppError.Errors.inValidResponse.message)
                    resolver.reject(error)
                    return
                }
                
                // fulfill
                resolver.fulfill(result.feeds)
            }
        }
    }
    
    func fetchMoreFeeds(with name: String) -> Promise<[Feed]> {
        return Promise { resolver in
            Router.fetchMoreFeeds(name).request { (response: DataResponse<SuccessResponse>) in
                guard response.error == nil else {
                    resolver.reject(response.error!)
                    return
                }
                guard let result = response.result.value else {
                    let error = AppError(1001, domain: "Error", message: AppError.Errors.inValidResponse.message)
                    resolver.reject(error)
                    return
                }
                
                // fulfill
                resolver.fulfill(result.feeds)
            }
        }
    }
}

extension FeedsAPIService {
    class SuccessResponse: Object {
        var feeds = [Feed]()
        
        override func mapping(map: Map) {
            feeds <- map["data.children"]
        }
    }
}

extension FeedsAPIService {
    enum Router: Requestable {
        case fetchFeeds
        case fetchMoreFeeds(String)
        
        var method: HTTPMethod {
            return .get
        }
        var path: PathPattern? {
            return .feeds
        }
        var encoding: EncodingType {
            return .queryStringEncoded
        }
        var parameters: Parameters? {
            switch self {
            case .fetchFeeds: return nil
            case .fetchMoreFeeds(let name): return ["after": name]
            }
        }
    }
}
