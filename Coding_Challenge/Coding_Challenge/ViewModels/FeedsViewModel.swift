//
//  FeedsViewModel.swift
//  Coding_Challenge
//
//  Created by Waqas Haider Sheikh on 15/09/2021.
//

import Foundation
import PromiseKit

class FeedsViewModel {
    private let feedsAPIService = FeedsAPIService()
    
    func loadFeeds() -> Promise<[Feed]> {
        return feedsAPIService.fetchFeeds()
    }
    
    func loadMoreFeeds(with name: String) -> Promise<[Feed]> {
        return feedsAPIService.fetchMoreFeeds(with: name)
    }
}
