//
//  PathPattern.swift
//  ISAssessment
//
//  Created by Waqas Haider Sheikh on 11/12/2020.
//  Copyright © 2020 Waqas Haider Sheikh. All rights reserved.
//

import Foundation

enum PathPattern {
    case feeds
    
    var name: String {
        switch self {
        case .feeds: return ".json"
        }
    }
}
