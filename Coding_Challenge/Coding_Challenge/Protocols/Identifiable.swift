//
//  Identifiable.swift
//  ISAssessment
//
//  Created by Waqas Haider on 11/12/2020.
//  Copyright © 2020 Waqas Haider Sheikh. All rights reserved.
//

import Foundation
import UIKit

protocol Identifiable {
    static var identifier: String { get }
}

extension UIResponder: Identifiable {
    static var identifier: String {
        return "\(self)"
    }
}
