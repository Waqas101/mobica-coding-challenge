//
//  Configuration.swift
//  ISAssessment
//
//  Created by Waqas Haider Sheikh on 11/12/2020.
//  Copyright © 2020 Waqas Haider Sheikh. All rights reserved.
//

import Foundation
import ObjectMapper

class Configuration {
    static let current = Configuration()
    
    private init() {
        
        // load all configurations one time
        all = Bundle.main.infoDictionary?["Configuration"] as? JSONDictionary ?? [:]
    }
    // all configuration keys from info Plist
    var all = JSONDictionary()
    
    // environment
    var environment: Environment {
        guard let environment = Environment(JSON: all["Environment"] as? JSONDictionary ?? [:]) else {
            fatalError("😂😜 ADD Environment TO YOUR CONFIGURATION FILE!!!")
        }
        return environment
    }
    
    // current scheme. by default it's development
    var scheme: Scheme {
        return Configuration.current.environment.scheme ?? .Development
    }
    
    // base URL
    var baseURL: URL? {
        guard let `protocol` = Configuration.current.environment.protocol, let host = Configuration.current.environment.host else {
            fatalError("😂😜 ADD HOST AND PROTOCOL TO YOUR CONFIGURATION FILE!!!")
        }
        
        var urlString = `protocol` + "://" + host
        
        // for every host there may not have deployment
        if let deployment = Configuration.current.environment.deployment, !deployment.isEmpty {
            urlString += "/" + deployment + "/"
        }
        
        return URL(string: urlString)
    }
}

extension Configuration {
    enum Scheme: String {
        case Development
    }
}

extension Configuration {
    
    class Environment: Object {
        
        // URL's protocol (http/https)
        var `protocol`: String?
        
        // host address
        var host: String?
        
        // deployment
        var deployment: String?
        
        // current scheme
        var scheme: Scheme?
        
        override func mapping(map: Map) {
            `protocol` <- map["protocol"]
            host       <- map["host"]
            deployment <- map["deployment"]
            scheme     <- (map["scheme"], EnumTransform<Scheme>())
        }
    }
}

