//
//  Coding_ChallengeTests.swift
//  Coding_ChallengeTests
//
//  Created by Waqas Haider Sheikh on 15/09/2021.
//

import XCTest
@testable import Coding_Challenge
import PromiseKit

class Coding_ChallengeTests: XCTestCase {
    
    let timeout = 125.0
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testFetchFeeds() {
        let feedsAPIService = FeedsAPIService()
        let expectation = expectation(description: "Fetch Feeds")
        
        feedsAPIService.fetchFeeds().done { feeds in
            XCTAssert(true)
            expectation.fulfill()
        }.catch { error in
            XCTAssert(false)
        }
        wait(for: [expectation], timeout: timeout)
    }
}
